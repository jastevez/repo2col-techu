function onSubmit(e) {
    console.log(e);
}

function onSaveData() {
    const name = document.getElementById("name");
    const email = document.getElementById("email");
    const dni = document.getElementById("dni");
    const data = {
        name: name.value,
        email: email.value,
        dni: dni.value
    };
    localStorage.setItem("user", JSON.stringify(data));
    //sessionStorage.setItem("user", el.value);
}

function onRetrieveData() {
    var user = JSON.parse(localStorage.getItem("user"));
    document.getElementById("name").value = user.name;
    document.getElementById("email").value = user.email;
    document.getElementById("dni").value = user.dni;
}